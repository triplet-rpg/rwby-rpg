#!/usr/bin/env bash

cd $(dirname $0)/..
source scripts/env.sh

set -eu
set -v

function usage()
{
    cat << EOF
Run script usage:
$0 [OPTIONS]

Builds configuration and HTML web-pages,
could be used for local testing.
Runs scripts from the CI repository.

Available options:
  --fast, -f   | Run script without setup
EOF
}

for ARG in "$@"
do
    case "${ARG}" in
        --fast|-f)
            ;;

        *)
            echo "Unsupported option: '${ARG}'" >&2
            usage
            exit 1
    esac
done

# Pre
bash scripts/prepare.sh -c "$@"
source "${VIRTUALENV_BIN}/activate"

# Script
bash ${BASH_OPTIONS} "${SCRIPTS_DIRECTORY}/mkdocs/doc_write-mkdocs-conf.sh"
bash ${BASH_OPTIONS} "${SCRIPTS_DIRECTORY}/mkdocs/doc_generate.sh"

echo Success!
