# GRIN - World of Remnant RPG
![remnant-map-large.png]

GRIN -- Game of Roleplay, Immersion & Narrative -- ролевая система,
основанная на системах Мира Тьмы и Троице, посвящённая обитателям мира Ремнант
и вдохновлённая таким анимационным произведением, как RWBY
(в мире которого, собственно, всё и происходит).

### Содержание:
 - [Создание и развитие персонажа][character-creation.md]:
   Общее описание о том, как создавать и развивать игрового персонажа
 - [Базовые правила][common-rules.md]:
   Общее описание о том, как вообще в это играть
 - [Атрибуты и аспекты][attributes.md]:
   Описание и примеры использования Героических, Небоевых и Боевых аспектов персонажа
 - [Боевые действия][combat-actions.md]:
   Правила для описания и ведения боевых ситуаций
 - [Ресурсы][resources.md]:
   *~~Здоровье, Броня, Деньги.~~*
   Здоровье, Аура, Даст
 - [Список изменений][patch-notes.md]:
   Полная история изменений системы

*Версия 1.1.2*

<!-- Links -->
[attributes.md]: ./docs/attributes.md "Атрибуты и аспекты"
[character-creation.md]: ./docs/character-creation.md "Создание и развитие персонажа"
[combat-actions.md]: ./docs/combat-actions.md "Боевые действия"
[common-rules.md]: ./docs/common-rules.md "Базовые правила"
[patch-notes.md]: ./docs/patch-notes.md "Список изменений"
[remnant-map-large.png]: ./docs/img/remnant-map-large.png "Карта Ремнанта"
[resources.md]: ./docs/resources.md "Ресурсы"
